package py.com.systvent.view;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import javax.swing.*;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import py.com.systven.utilitarios.*;
//import registros.*;
/*
 * VentanaPrincipal2.java
 *
 * Created on Jul 3, 2015, 6:21:52 PM
 */
/**
 *
 * @author hp
 */
public class VentanaPrincipal2 extends javax.swing.JFrame {

    /** Creates new form VentanaPrincipal2 */
    data mostrar_datos;
    String set_look = "com.sun.java.swing.plaf.metal.MetalLookAndFeel";
    
    public VentanaPrincipal2() {
        initComponents();
        
        mostrar_datos = new data();
        mostrar_datos.el_dato();//primero tengo que ejecutar este metodo para después ejecutar el resto
        lbl_data.setText("Hoy es "+mostrar_datos.dia_semana+ " "+mostrar_datos.dia+" de "+mostrar_datos.mes+" del año "+mostrar_datos.anho);
        timer1.start();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        timer1 = new org.netbeans.examples.lib.timerbean.Timer();
        jPanel1 = new javax.swing.JPanel();
        btnCliente = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lbl_data = new javax.swing.JLabel();
        lbl_hora = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        menuRegistro = new javax.swing.JMenu();
        mitem_cliente = new javax.swing.JMenuItem();
        mitem_direccion = new javax.swing.JMenuItem();
        mitem_telefono = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        mitem_tipoPago = new javax.swing.JMenuItem();
        mitem_funcionario = new javax.swing.JMenuItem();
        mitem_cargo = new javax.swing.JMenuItem();
        mitem_depart = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        mitem_proveedores = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        mitem_producto = new javax.swing.JMenuItem();
        mitem_unidad = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        mitem_salir = new javax.swing.JMenuItem();
        menuConsultas = new javax.swing.JMenu();
        mitem_ventas = new javax.swing.JMenuItem();
        mitem_presupuesto = new javax.swing.JMenuItem();
        menuReportes = new javax.swing.JMenu();
        mitem_reporteVentas = new javax.swing.JMenuItem();
        menuUtilitarios = new javax.swing.JMenu();
        mitem_lookNimbus = new javax.swing.JMenuItem();
        mitem_lookLiquid = new javax.swing.JMenuItem();
        menuSeguridad = new javax.swing.JMenu();
        jmi_cambiar_contraseña = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();

        timer1.addTimerListener(new org.netbeans.examples.lib.timerbean.TimerListener() {
            public void onTime(java.awt.event.ActionEvent evt) {
                timer1OnTime(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sistema de Ventas");

        jPanel1.setBackground(new java.awt.Color(-4144960,true));

        btnCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/cliente1.png"))); // NOI18N
        btnCliente.setText("Cliente");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnCliente)
                .addContainerGap(522, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnCliente)
                .addContainerGap(66, Short.MAX_VALUE))
        );

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/politecnica.png"))); // NOI18N

        lbl_data.setText("Fecha: ");

        lbl_hora.setText("Hora: ");

        menuRegistro.setMnemonic('R');
        menuRegistro.setText("Registros");

        mitem_cliente.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        mitem_cliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/clientes.png"))); // NOI18N
        mitem_cliente.setMnemonic('C');
        mitem_cliente.setText("Cliente");
        mitem_cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mitem_clienteActionPerformed(evt);
            }
        });
        menuRegistro.add(mitem_cliente);

        mitem_direccion.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        mitem_direccion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/ciudad.png"))); // NOI18N
        mitem_direccion.setMnemonic('D');
        mitem_direccion.setText("Direccion");
        menuRegistro.add(mitem_direccion);

        mitem_telefono.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.ALT_MASK));
        mitem_telefono.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/barrio.png"))); // NOI18N
        mitem_telefono.setText("Telefono");
        menuRegistro.add(mitem_telefono);
        menuRegistro.add(jSeparator2);
        menuRegistro.add(jSeparator3);

        mitem_tipoPago.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.ALT_MASK));
        mitem_tipoPago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/icon-money.png"))); // NOI18N
        mitem_tipoPago.setText("Tipo de Pago");
        menuRegistro.add(mitem_tipoPago);

        mitem_funcionario.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.ALT_MASK));
        mitem_funcionario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/funcionario.png"))); // NOI18N
        mitem_funcionario.setMnemonic('F');
        mitem_funcionario.setText("Funcionarios");
        mitem_funcionario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mitem_funcionarioActionPerformed(evt);
            }
        });
        menuRegistro.add(mitem_funcionario);

        mitem_cargo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        mitem_cargo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/cargo.png"))); // NOI18N
        mitem_cargo.setMnemonic('o');
        mitem_cargo.setText("Cargos");
        mitem_cargo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mitem_cargoActionPerformed(evt);
            }
        });
        menuRegistro.add(mitem_cargo);

        mitem_depart.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        mitem_depart.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/world.png"))); // NOI18N
        mitem_depart.setMnemonic('B');
        mitem_depart.setText("Departamento");
        mitem_depart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mitem_departActionPerformed(evt);
            }
        });
        menuRegistro.add(mitem_depart);
        menuRegistro.add(jSeparator4);

        mitem_proveedores.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_MASK));
        mitem_proveedores.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/user-32.png"))); // NOI18N
        mitem_proveedores.setMnemonic('P');
        mitem_proveedores.setText("Proveedores");
        menuRegistro.add(mitem_proveedores);
        menuRegistro.add(jSeparator5);

        mitem_producto.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        mitem_producto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/folder_camera.png"))); // NOI18N
        mitem_producto.setMnemonic('i');
        mitem_producto.setText("Producto");
        menuRegistro.add(mitem_producto);

        mitem_unidad.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.SHIFT_MASK));
        mitem_unidad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/wrench_orange.png"))); // NOI18N
        mitem_unidad.setText("Unidad");
        mitem_unidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mitem_unidadActionPerformed(evt);
            }
        });
        menuRegistro.add(mitem_unidad);
        menuRegistro.add(jSeparator1);

        mitem_salir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        mitem_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/salir.png"))); // NOI18N
        mitem_salir.setMnemonic('s');
        mitem_salir.setText("Salir");
        mitem_salir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mitem_salirMouseClicked(evt);
            }
        });
        mitem_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mitem_salirActionPerformed(evt);
            }
        });
        menuRegistro.add(mitem_salir);

        jMenuBar1.add(menuRegistro);

        menuConsultas.setMnemonic('n');
        menuConsultas.setText("Movimientos");
        menuConsultas.setToolTipText("");

        mitem_ventas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.SHIFT_MASK));
        mitem_ventas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/getmoney.png"))); // NOI18N
        mitem_ventas.setText("Ventas");
        mitem_ventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mitem_ventasActionPerformed(evt);
            }
        });
        menuConsultas.add(mitem_ventas);

        mitem_presupuesto.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        mitem_presupuesto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/money.png"))); // NOI18N
        mitem_presupuesto.setText("Presupuesto");
        mitem_presupuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mitem_presupuestoActionPerformed(evt);
            }
        });
        menuConsultas.add(mitem_presupuesto);

        jMenuBar1.add(menuConsultas);

        menuReportes.setMnemonic('r');
        menuReportes.setText("Reportes");

        mitem_reporteVentas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_MASK));
        mitem_reporteVentas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/relatorio.png"))); // NOI18N
        mitem_reporteVentas.setText("Reporte de Ventas");
        mitem_reporteVentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mitem_reporteVentasActionPerformed(evt);
            }
        });
        menuReportes.add(mitem_reporteVentas);

        jMenuBar1.add(menuReportes);

        menuUtilitarios.setMnemonic('U');
        menuUtilitarios.setText("Utilitarios");

        mitem_lookNimbus.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        mitem_lookNimbus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/application_double.png"))); // NOI18N
        mitem_lookNimbus.setText("Look And Feel - Nimbus");
        mitem_lookNimbus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mitem_lookNimbusActionPerformed(evt);
            }
        });
        menuUtilitarios.add(mitem_lookNimbus);

        mitem_lookLiquid.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        mitem_lookLiquid.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/table.png"))); // NOI18N
        mitem_lookLiquid.setText("Look And Feel - Liquid");
        mitem_lookLiquid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mitem_lookLiquidActionPerformed(evt);
            }
        });
        menuUtilitarios.add(mitem_lookLiquid);

        jMenuBar1.add(menuUtilitarios);

        menuSeguridad.setMnemonic('S');
        menuSeguridad.setText("Seguridad");
        menuSeguridad.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menuSeguridadMouseClicked(evt);
            }
        });

        jmi_cambiar_contraseña.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jmi_cambiar_contraseña.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/lock.png"))); // NOI18N
        jmi_cambiar_contraseña.setText("Cambiar contraseña actual");
        jmi_cambiar_contraseña.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmi_cambiar_contraseñaActionPerformed(evt);
            }
        });
        menuSeguridad.add(jmi_cambiar_contraseña);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/py/com/systven/graficos/key_delete.png"))); // NOI18N
        jMenuItem2.setText("Nivel de acceso");
        menuSeguridad.add(jMenuItem2);

        jMenuBar1.add(menuSeguridad);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbl_data)
                            .addComponent(lbl_hora)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(28, 28, 28))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 61, Short.MAX_VALUE)
                .addComponent(lbl_data)
                .addGap(14, 14, 14)
                .addComponent(lbl_hora)
                .addContainerGap())
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-675)/2, (screenSize.height-469)/2, 675, 469);
    }// </editor-fold>//GEN-END:initComponents

    private void menuSeguridadMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menuSeguridadMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_menuSeguridadMouseClicked

    private void mitem_lookNimbusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mitem_lookNimbusActionPerformed
        // TODO add your handling code here:
        set_look = "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel";
        lookandfeel();
    }//GEN-LAST:event_mitem_lookNimbusActionPerformed

    private void mitem_presupuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mitem_presupuestoActionPerformed
        // TODO add your handling code here:
        String args[] = new String [1];
        args[0] = "Movimiento - Registro de Presupuesto";
        MovimientoPresupuestoVenta.main(args);        
    }//GEN-LAST:event_mitem_presupuestoActionPerformed

    private void mitem_ventasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mitem_ventasActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_mitem_ventasActionPerformed

    private void mitem_unidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mitem_unidadActionPerformed
        // TODO add your handling code here:
       
    }//GEN-LAST:event_mitem_unidadActionPerformed

    private void mitem_departActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mitem_departActionPerformed
        // TODO add your handling code here:
    
    }//GEN-LAST:event_mitem_departActionPerformed

    private void mitem_funcionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mitem_funcionarioActionPerformed
        // TODO add your handling code here:
    
       
    }//GEN-LAST:event_mitem_funcionarioActionPerformed

    private void mitem_clienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mitem_clienteActionPerformed
        // TODO add your handling code here:
     
    }//GEN-LAST:event_mitem_clienteActionPerformed

    private void mitem_lookLiquidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mitem_lookLiquidActionPerformed
        // TODO add your handling code here:
       set_look = "com.birosoft.liquid.LiquidLookAndFeel";
       lookandfeel();
    }//GEN-LAST:event_mitem_lookLiquidActionPerformed

    private void mitem_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mitem_salirActionPerformed
        if (JOptionPane.showConfirmDialog(rootPane, "¿Desea realmente salir del sistema?",
                "Salir del sistema", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) 
        System.exit(0);
// TODO add your handling code here:
    }//GEN-LAST:event_mitem_salirActionPerformed

    private void mitem_cargoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mitem_cargoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mitem_cargoActionPerformed

    private void jmi_cambiar_contraseñaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmi_cambiar_contraseñaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jmi_cambiar_contraseñaActionPerformed

    private void mitem_reporteVentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mitem_reporteVentasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mitem_reporteVentasActionPerformed

private void timer1OnTime(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_timer1OnTime
// TODO add your handling code here:
    mostrar_datos.leer_hora();
    lbl_hora.setText("Hora Actual: "+ mostrar_datos.hora);
}//GEN-LAST:event_timer1OnTime

private void mitem_salirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mitem_salirMouseClicked
// TODO add your handling code here:
    System.exit(0);
}//GEN-LAST:event_mitem_salirMouseClicked

    public void lookandfeel() {
        try {
        //La variable set look tiene que estar definida arriba como una variable String
            UIManager.setLookAndFeel(set_look);
            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception error) {
            JOptionPane.showMessageDialog(null, error);
            
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(VentanaPrincipal2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(VentanaPrincipal2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(VentanaPrincipal2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(VentanaPrincipal2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new VentanaPrincipal2().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCliente;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JMenuItem jmi_cambiar_contraseña;
    private javax.swing.JLabel lbl_data;
    private javax.swing.JLabel lbl_hora;
    private javax.swing.JMenu menuConsultas;
    private javax.swing.JMenu menuRegistro;
    private javax.swing.JMenu menuReportes;
    private javax.swing.JMenu menuSeguridad;
    private javax.swing.JMenu menuUtilitarios;
    private javax.swing.JMenuItem mitem_cargo;
    private javax.swing.JMenuItem mitem_cliente;
    private javax.swing.JMenuItem mitem_depart;
    private javax.swing.JMenuItem mitem_direccion;
    private javax.swing.JMenuItem mitem_funcionario;
    private javax.swing.JMenuItem mitem_lookLiquid;
    private javax.swing.JMenuItem mitem_lookNimbus;
    private javax.swing.JMenuItem mitem_presupuesto;
    private javax.swing.JMenuItem mitem_producto;
    private javax.swing.JMenuItem mitem_proveedores;
    private javax.swing.JMenuItem mitem_reporteVentas;
    private javax.swing.JMenuItem mitem_salir;
    private javax.swing.JMenuItem mitem_telefono;
    private javax.swing.JMenuItem mitem_tipoPago;
    private javax.swing.JMenuItem mitem_unidad;
    private javax.swing.JMenuItem mitem_ventas;
    private org.netbeans.examples.lib.timerbean.Timer timer1;
    // End of variables declaration//GEN-END:variables
}
