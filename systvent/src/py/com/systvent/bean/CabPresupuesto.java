/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.systvent.bean;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author hp
 */
@Entity
@Table(name = "cab_presupuesto", catalog = "sistvent", schema = "")
@NamedQueries({
    @NamedQuery(name = "CabPresupuesto.findAll", query = "SELECT c FROM CabPresupuesto c"),
    @NamedQuery(name = "CabPresupuesto.findByCodPresupuesto", query = "SELECT c FROM CabPresupuesto c WHERE c.codPresupuesto = :codPresupuesto"),
    @NamedQuery(name = "CabPresupuesto.findByFchPresCab", query = "SELECT c FROM CabPresupuesto c WHERE c.fchPresCab = :fchPresCab"),
    @NamedQuery(name = "CabPresupuesto.findByVlrPresuCab", query = "SELECT c FROM CabPresupuesto c WHERE c.vlrPresuCab = :vlrPresuCab"),
    @NamedQuery(name = "CabPresupuesto.findByCodCliente", query = "SELECT c FROM CabPresupuesto c WHERE c.codCliente = :codCliente"),
    @NamedQuery(name = "CabPresupuesto.findByCodFuncionario", query = "SELECT c FROM CabPresupuesto c WHERE c.codFuncionario = :codFuncionario")})
public class CabPresupuesto implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_presupuesto")
    private Integer codPresupuesto;
    @Column(name = "fch_pres_cab")
    @Temporal(TemporalType.DATE)
    private Date fchPresCab;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "vlr_presu_cab")
    private Double vlrPresuCab;
    @Column(name = "cod_cliente")
    private Integer codCliente;
//    @Column(name = "cod_funcionario")
//    private Integer codFuncionario;
    @JoinColumn(name = "cod_funcionario", referencedColumnName ="cod_funcionario" ) 
    @ManyToOne //con esta anotación estamos diciendo diciendo que tenemos muchos Presupuestos para este funcionario.
    private Funcionario codFuncionario;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cabPresupuesto")
    private List<DetPresupuesto> detPresupuestoList;

    public CabPresupuesto() {
    }

    public CabPresupuesto(Integer codPresupuesto) {
        this.codPresupuesto = codPresupuesto;
    }

    public Integer getCodPresupuesto() {
        return codPresupuesto;
    }

    public void setCodPresupuesto(Integer codPresupuesto) {
        Integer oldCodPresupuesto = this.codPresupuesto;
        this.codPresupuesto = codPresupuesto;
        changeSupport.firePropertyChange("codPresupuesto", oldCodPresupuesto, codPresupuesto);
    }

    public Date getFchPresCab() {
        return fchPresCab;
    }

    public void setFchPresCab(Date fchPresCab) {
        Date oldFchPresCab = this.fchPresCab;
        this.fchPresCab = fchPresCab;
        changeSupport.firePropertyChange("fchPresCab", oldFchPresCab, fchPresCab);
    }

    public Double getVlrPresuCab() {
        return vlrPresuCab;
    }

    public void setVlrPresuCab(Double vlrPresuCab) {
        Double oldVlrPresuCab = this.vlrPresuCab;
        this.vlrPresuCab = vlrPresuCab;
        changeSupport.firePropertyChange("vlrPresuCab", oldVlrPresuCab, vlrPresuCab);
    }

    public Integer getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(Integer codCliente) {
        Integer oldCodCliente = this.codCliente;
        this.codCliente = codCliente;
        changeSupport.firePropertyChange("codCliente", oldCodCliente, codCliente);
    }

    public Funcionario getCodFuncionario() {
        return codFuncionario;
    }

    public void setCodFuncionario(Funcionario codFuncionario) {
//        this.codFuncionario = codFuncionario;
        Funcionario oldCodFuncionario = this.codFuncionario; 
        this.codFuncionario = codFuncionario; 
        changeSupport.firePropertyChange("codFuncionario", oldCodFuncionario, codFuncionario);
    }

//    public Integer getCodFuncionario() {
//        return codFuncionario;
//    }
//
//    public void setCodFuncionario(Integer codFuncionario) {
//        Integer oldCodFuncionario = this.codFuncionario;
//        this.codFuncionario = codFuncionario;
//        changeSupport.firePropertyChange("codFuncionario", oldCodFuncionario, codFuncionario);
//    }

    public List<DetPresupuesto> getDetPresupuestoList() {
        return detPresupuestoList;
    }

    public void setDetPresupuestoList(List<DetPresupuesto> detPresupuestoList) {
        this.detPresupuestoList = detPresupuestoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codPresupuesto != null ? codPresupuesto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CabPresupuesto)) {
            return false;
        }
        CabPresupuesto other = (CabPresupuesto) object;
        if ((this.codPresupuesto == null && other.codPresupuesto != null) || (this.codPresupuesto != null && !this.codPresupuesto.equals(other.codPresupuesto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.systvent.bean.CabPresupuesto[ codPresupuesto=" + codPresupuesto + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
