/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.systvent.bean;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author hp
 */
@Embeddable
public class VentaDetPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "cod_venta_cab")
    private int codVentaCab;
    @Basic(optional = false)
    @Column(name = "cod_producto")
    private String codProducto;

    public VentaDetPK() {
    }

    public VentaDetPK(int codVentaCab, String codProducto) {
        this.codVentaCab = codVentaCab;
        this.codProducto = codProducto;
    }

    public int getCodVentaCab() {
        return codVentaCab;
    }

    public void setCodVentaCab(int codVentaCab) {
        this.codVentaCab = codVentaCab;
    }

    public String getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) codVentaCab;
        hash += (codProducto != null ? codProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentaDetPK)) {
            return false;
        }
        VentaDetPK other = (VentaDetPK) object;
        if (this.codVentaCab != other.codVentaCab) {
            return false;
        }
        if ((this.codProducto == null && other.codProducto != null) || (this.codProducto != null && !this.codProducto.equals(other.codProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.systvent.bean.VentaDetPK[ codVentaCab=" + codVentaCab + ", codProducto=" + codProducto + " ]";
    }
    
}
