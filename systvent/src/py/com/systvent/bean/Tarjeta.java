/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.systvent.bean;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author hp
 */
@Entity
@Table(name = "tarjeta", catalog = "sistvent", schema = "")
@NamedQueries({
    @NamedQuery(name = "Tarjeta.findAll", query = "SELECT t FROM Tarjeta t"),
    @NamedQuery(name = "Tarjeta.findByCodTarjeta", query = "SELECT t FROM Tarjeta t WHERE t.codTarjeta = :codTarjeta"),
    @NamedQuery(name = "Tarjeta.findByNombreTarjeta", query = "SELECT t FROM Tarjeta t WHERE t.nombreTarjeta = :nombreTarjeta"),
    @NamedQuery(name = "Tarjeta.findByTazaTarjeta", query = "SELECT t FROM Tarjeta t WHERE t.tazaTarjeta = :tazaTarjeta")})
public class Tarjeta implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_tarjeta")
    private Integer codTarjeta;
    @Column(name = "nombre_tarjeta")
    private String nombreTarjeta;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "taza_tarjeta")
    private Double tazaTarjeta;

    public Tarjeta() {
    }

    public Tarjeta(Integer codTarjeta) {
        this.codTarjeta = codTarjeta;
    }

    public Integer getCodTarjeta() {
        return codTarjeta;
    }

    public void setCodTarjeta(Integer codTarjeta) {
        Integer oldCodTarjeta = this.codTarjeta;
        this.codTarjeta = codTarjeta;
        changeSupport.firePropertyChange("codTarjeta", oldCodTarjeta, codTarjeta);
    }

    public String getNombreTarjeta() {
        return nombreTarjeta;
    }

    public void setNombreTarjeta(String nombreTarjeta) {
        String oldNombreTarjeta = this.nombreTarjeta;
        this.nombreTarjeta = nombreTarjeta;
        changeSupport.firePropertyChange("nombreTarjeta", oldNombreTarjeta, nombreTarjeta);
    }

    public Double getTazaTarjeta() {
        return tazaTarjeta;
    }

    public void setTazaTarjeta(Double tazaTarjeta) {
        Double oldTazaTarjeta = this.tazaTarjeta;
        this.tazaTarjeta = tazaTarjeta;
        changeSupport.firePropertyChange("tazaTarjeta", oldTazaTarjeta, tazaTarjeta);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTarjeta != null ? codTarjeta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tarjeta)) {
            return false;
        }
        Tarjeta other = (Tarjeta) object;
        if ((this.codTarjeta == null && other.codTarjeta != null) || (this.codTarjeta != null && !this.codTarjeta.equals(other.codTarjeta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.systvent.bean.Tarjeta[ codTarjeta=" + codTarjeta + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
