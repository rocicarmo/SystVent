/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.systvent.bean;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author hp
 */
@Entity
@Table(name = "tipo_pago", catalog = "sistvent", schema = "")
@NamedQueries({
    @NamedQuery(name = "TipoPago.findAll", query = "SELECT t FROM TipoPago t"),
    @NamedQuery(name = "TipoPago.findByCodTipoPago", query = "SELECT t FROM TipoPago t WHERE t.codTipoPago = :codTipoPago"),
    @NamedQuery(name = "TipoPago.findByDescTipoPago", query = "SELECT t FROM TipoPago t WHERE t.descTipoPago = :descTipoPago")})
public class TipoPago implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_tipo_pago")
    private Integer codTipoPago;
    @Column(name = "desc_tipo_pago")
    private String descTipoPago;

    public TipoPago() {
    }

    public TipoPago(Integer codTipoPago) {
        this.codTipoPago = codTipoPago;
    }

    public Integer getCodTipoPago() {
        return codTipoPago;
    }

    public void setCodTipoPago(Integer codTipoPago) {
        Integer oldCodTipoPago = this.codTipoPago;
        this.codTipoPago = codTipoPago;
        changeSupport.firePropertyChange("codTipoPago", oldCodTipoPago, codTipoPago);
    }

    public String getDescTipoPago() {
        return descTipoPago;
    }

    public void setDescTipoPago(String descTipoPago) {
        String oldDescTipoPago = this.descTipoPago;
        this.descTipoPago = descTipoPago;
        changeSupport.firePropertyChange("descTipoPago", oldDescTipoPago, descTipoPago);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTipoPago != null ? codTipoPago.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoPago)) {
            return false;
        }
        TipoPago other = (TipoPago) object;
        if ((this.codTipoPago == null && other.codTipoPago != null) || (this.codTipoPago != null && !this.codTipoPago.equals(other.codTipoPago))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.systvent.bean.TipoPago[ codTipoPago=" + codTipoPago + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
