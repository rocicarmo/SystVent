/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.systvent.renderizar;

import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import py.com.systvent.bean.TipoPago;

/**
 *
 * @author hp
 */
public class TipoPagoListRenderizar extends DefaultListCellRenderer{
    @Override 
    public Component getListCellRendererComponent( JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus); 
        if (value instanceof TipoPago){ //Si el valor es una instancia de Funcionario 
            TipoPago tp = (TipoPago) value;//asigno el valor del funcionario 
            setText(tp.getDescTipoPago());//asignamos en nuestro caso el valor de la descripción de la unidad 
        }
        return this; 
    }
}
