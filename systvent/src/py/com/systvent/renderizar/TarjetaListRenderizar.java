/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.systvent.renderizar;

import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import py.com.systvent.bean.Tarjeta;

/**
 *
 * @author hp
 */
public class TarjetaListRenderizar extends DefaultListCellRenderer{
    @Override 
    public Component getListCellRendererComponent( JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus); 
        if (value instanceof Tarjeta){ //Si el valor es una instancia de Funcionario 
            Tarjeta t = (Tarjeta) value;//asigno el valor del funcionario 
            setText(t.getNombreTarjeta());//asignamos en nuestro caso el valor de la descripción de la unidad 
        }
        return this; 
    }
}
